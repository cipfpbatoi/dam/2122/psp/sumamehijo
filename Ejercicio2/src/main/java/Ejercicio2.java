import java.io.IOException;
import java.util.Scanner;

/**
 *
 * 2. Realiza la clase Ejercicio2 ejecuta el proceso hijo en modo comando
 * pasándole los números 1 7 25 y 20 como argumentos. Utiliza para ellos
 * los métodos de la clase Process que nos permitirían mostrar el resultado
 * de la ejecución del comando tanto si se ha ejecutado correctamente como
 * si ha habido algún problema.
 *
 */
public class Ejercicio2 {

    public static void main(String[] args) {

        String commando = "java -jar SumaMeHijo.jar 1 7 25 20";

        ProcessBuilder pb = new ProcessBuilder(commando.split(" "));
        /**
         * Nos aseguramos de que se muestran los errores
         */
        pb.redirectErrorStream(true);

        try {
            Process hijo = pb.start();

            Scanner sc = new Scanner(hijo.getInputStream());
            /**
             * Esta línea no es absolutamente necesaria, pero
             * como es un programa que se ejecuta en modo comando
             * pues podemos esperar a que finalice y luego
             * imprimir la salida
             */
            hijo.waitFor();

            while (sc.hasNextLine()){
                System.out.println(sc.nextLine());
            }

        } catch (IOException|InterruptedException e) {
            e.printStackTrace();
        }

    }

}
