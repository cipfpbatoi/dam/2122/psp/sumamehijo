import java.io.IOException;

/**
 * 1. Realiza la clase Ejercicio1 que ejecute el proceso hijo
 * en modo interactivo sin argumentos y que reenvíe al usuario
 * todas las entradas y salidas del proceso hijo mediante métodos
 * de la clase ProcessBuilder  Asegúrate de que se muestra
 * correctamente la salida cuando hay un error.
 *
 */
public class Ejercicio1 {

    public static void main(String[] args) {

        String commando = "java -jar SumaMeHijo.jar -i";

        ProcessBuilder pb = new ProcessBuilder(commando.split(" "));
        /**
         * La línea inheritIO asegura que se reenvian todas las
         * salidas.
         */
        pb.inheritIO();

        try {
            Process hijo = pb.start();
            hijo.waitFor();
        } catch (IOException|InterruptedException e) {
            e.printStackTrace();
        }

    }

}
