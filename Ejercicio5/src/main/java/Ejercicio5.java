import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 5. Realiza la clase Ejercicio5 que acepte como parámetros una lista
 * de números de longitud par y opcionalmente, un archivo donde vayamos
 * a guardar la salida mediante mediante [-f archivoSalida.txt].
 * Por simplicidad,  el fichero, en caso de estar este fichero irá al principio.
 * La clase Ejercicio4 ha de ejecutar el proceso hijo en modo comando
 * por cada dos valores pasado como entrada y mostrar una lista con los resultados
 * por la salida.
 *
 * Recuerda que debe comprobarse si el proceso se ha ejecutado correctamente (3p):
 *
 * Ejemplo de ejecución: java -jar Ejercicio5.jar -f ficheroSalida.txt 1 5 9 3 12 5  ( La salida que se muestra es el contenido del fichero (Si no hubiéramos pasado el fichero, esta seria la salida estándar):
 * 6
 * 12
 * 17
 */
public class Ejercicio5 {

    public static void main(String[] args) {

        if (args.length % 2 == 1){
            System.err.println("El número de parámetros ha de ser par");
            System.exit(1);
        }

        String comando = "java -jar SumaMeHijo.jar";

        String archivoSalida = args[0].equals("-f") ? args[1] : null;

        File archivoSalidaFile = null;

        /**
         * Como hacemos que el proceso haga append, si
         * lo creamos, lo tenemos que eliminar para
         * las sucesivas pruebas
         */
        if (archivoSalida != null ){
            archivoSalidaFile = new File(archivoSalida);

            if (archivoSalidaFile.exists()){
                archivoSalidaFile.delete();
            }

        }

        ArrayList<String> comandos = new ArrayList<String>();

        for (String com : comando.split(" ")){
            comandos.add(com);
        }

        int i = 0;

        if (archivoSalida != null)
        {
            i = 2;
        }

        while (i<args.length){

            comandos.add(args[i]);
            comandos.add(args[i+1]);

            ProcessBuilder pb = new ProcessBuilder(comandos);

            if (archivoSalida != null)
            {
                pb.redirectOutput(ProcessBuilder.Redirect.appendTo(new File(archivoSalida)));
            } else {
                pb.inheritIO();
            }

            try {
                if (pb.start().waitFor() != 0){
                    System.err.println("El proceso no finalizó correctamente");
                }

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

            comandos.remove(args[i+1]);
            comandos.remove(args[i]);

            i+=2;

        }

    }

}
