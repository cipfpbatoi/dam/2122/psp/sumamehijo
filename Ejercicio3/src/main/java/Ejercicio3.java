import java.io.*;
import java.util.Scanner;

/**
 * 3. Realiza la Clase Ejercicio3 que ejecute el proceso hijo en modo interactivo sin
 * argumentos y que formatee la salida de forma que le dé mas información al usuario.
 * Una posible forma de hacerlo seria :
 *              Introduce un número:  3
 *              El total de la suma acumulada es: 3
 *              Introduce un número:  4
 *              El total de la suma acumulada es: 7
 *              Prueba también algún argumento que no sea válido.
 *              ¿Qué debería de pasar? ¿Qué está ocurriendo?
 *              Contesta a esta pregunta dentro de un comentario dentro de la Clase.
 *
 */
public class Ejercicio3 {

    public static void main(String[] args) {

        String commando = "java -jar SumaMeHijo.jar -i";

        ProcessBuilder pb = new ProcessBuilder(commando.split(" "));
        //pb.redirectErrorStream(true);

        /**
         * La salida de error no se está mostrando por lo que no
         * se informa al usuario.
         */

        try {
            Process hijo = pb.start();

            Scanner scannerHijo = new Scanner(hijo.getInputStream());

            OutputStream os = hijo.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bfw = new BufferedWriter(osw);

            PrintWriter pw = new PrintWriter(bfw);

            Scanner sc = new Scanner(System.in);

            String linea = null;
            System.out.println("Introduce un número:");
            while (!(linea = sc.nextLine()).equals("stop")){

                pw.println(linea);
                pw.flush();
                System.out.println("El resultado es:");
                System.out.println(scannerHijo.nextLine());
                System.out.println("Introduce un número:");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
