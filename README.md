# SumaMe Hijo

Esta es una posible solución al examen de PSP para el grupo de DAMA realizado
el viernes 29/10/21. 

Lo que se proporcionaba era un jar clase principal SumaMeHijo y se pedian
diversos ejercicios.

Cada ejercicio se ha realizado en un proyecto maven distinto para que se 
puedan revisar y probar de manera individual.