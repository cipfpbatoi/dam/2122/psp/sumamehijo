# SumaMeHijo

Esta es la ayuda del comando SumaMeHijo.jar. La funcionalidad este programa 
es devolvernos la suma de los parámetros **enteros** que le introduzcamos.

La aplicación tiene varios modos de funcionamiento, se detallan a continuación:

## Modo Comando 

Se ejecuta realiza lo que tenga que hacer y finaliza (no espera 
la entrada del usuario)

java -jar SumaMeHijo.jar: Muestra esta ayuda

java -jar SumaMeHijo.jar -h: Muestra esta ayuda

java -jar SumaMeHijo.jar <intParam1> <intParam2> ... <intParamN> imprime por la salida
estándar la suma de todos los números ENTEROS introducidos y finaliza con 
System.exit(0) en caso de que haya habido algún error, lo imprimirá por la salida de
ERROR estándar, mostrará la ayuda, y finalizará con un valor distinto de 0.

java -jar SumaMeHijo.jar 3 5 7
15

java -jar SumaMeHijo.jar 3 5 pepe
pepe no es un número entero
...Aquí se muestra la ayuda pero no se ha agregado para evitar recursividad...

# Modo Interactivo

Para entrar en el modo interactivo hemos de ejecutar el comando con el parámetro
-i: java -jar SumaMeHijo.jar -i

A continuación el programa esperará a que le introduzcamos un número Entero. Una 
vez se lo introduzcamos, imprimirá la suma de todos los números introducidos por
la salida ESTANDAR.

En el caso de que el número introducido no sea un número entero, devolverá un error
por la salida de ERROR estándar indicando el tipo de error y escribirá por la salida
ESTANDAR el valor actual acumulado.

Al introducir la palabra: stop el programa finalizará su ejecución.

Ejemplo de Ejecución:

java -jar SumaMeHijo.jar -i
3
3
4
7
2
9
pepe
pepe no es un entero
9
1
10
stop


