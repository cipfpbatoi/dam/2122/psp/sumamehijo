public class FormatOfArgumentsException extends NumberFormatException{

    public FormatOfArgumentsException(String s){
        super(s);
    }

    public FormatOfArgumentsException(){
        super();
    }

}
