import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class SumaMeHijo {

    private static final String ERROR = "error";
    public static final String STOP = "stop";

    private static final int EXIT_OK = 0;
    private static final int EXIT_ERROR_NAME_NOT_FOUND = 1;

    private static final int EXIT_ERROR_UNKNOWN = 10;
    private static final int EXIT_ERROR_WRONG_ARGS = 11;
    private static final int EXIT_ERROR_DICT_FILE_NOT_EXIST = 12;

    private static final String COMMAND_LINE_HELP = "-h";
    private static final String COMMAND_LINE_INTERACTIVE_MODE = "-i";

    private static final String JAR_RUN_HELP = "java -jar SumameHijo.jar";

    public static void main(String[] args) {

       int numberOfArgs = args.length;

       if (numberOfArgs == 0){
           ImprimirInstruccionesDeUso(System.err);
           System.exit(EXIT_ERROR_WRONG_ARGS);
       } else {
           if ( numberOfArgs == 1 && args[0].equals(COMMAND_LINE_INTERACTIVE_MODE)){
               EmpezarElModoInteractivo();
               System.exit(EXIT_OK);
           } else if (numberOfArgs == 1 && args[0].equals(COMMAND_LINE_HELP)){
               ImprimirInstruccionesDeUso(System.out);
               System.exit(EXIT_OK);
           } else {
               try {
                   System.out.println(SumaTodosLosArgumentos(args));
                   System.exit(EXIT_OK);
               } catch (FormatOfArgumentsException e) {
                   System.err.println(e.getMessage());
                   ImprimirInstruccionesDeUso(System.err);
                   System.exit(EXIT_ERROR_WRONG_ARGS);
               }
           }
       }

    }

    private static void EmpezarElModoInteractivo()
    {
        int total = 0;
        String linea = null;
        Scanner sc = new Scanner(System.in);

        while (!(linea = sc.nextLine()).equals(STOP)){

            try {
                total+=Integer.parseInt(linea);
            } catch (NumberFormatException e){
                System.err.printf("%s no es un entero",linea);
                System.err.println();
            }
            System.out.println(total);

        }
    }

    private static int SumaTodosLosArgumentos(String args[]) throws NumberFormatException{

        int resultado = 0;
        String currentArg = null; //Se crea para poderlo devolver en una posible excepcion

        try{
            for (String arg : args) {
                currentArg = arg;
                resultado+=Integer.parseInt(currentArg);
            }
        } catch (NumberFormatException e){
            throw new FormatOfArgumentsException(
                currentArg + " no es un número entero"
            );
        }

        return resultado;
    }

    private static void ImprimirInstruccionesDeUso(PrintStream ps)
    {
        try (InputStream is = SumaMeHijo.class.getResourceAsStream("readme.md");
             Scanner sc = new Scanner(is)){

            while (sc.hasNextLine()){
                ps.println(sc.nextLine());
            }

        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(EXIT_ERROR_UNKNOWN);
        }
    }

}
