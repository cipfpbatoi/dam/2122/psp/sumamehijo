import java.io.File;
import java.io.IOException;

/**
 * 4. Realiza una clase llamada Ejercicio4 a la que que le pasen como
 * argumento dos ficheros. El primer fichero contendrá una lista de
 * números, uno por línea y al finalizar la palabra stop, el otro
 * fichero será la salida del proceso SumaMeHijo cuando se ejecuta
 * en modo interactivo y se le pasan todos estos parámetros.
 * La clase Ejercicio4 debe de leer el fichero de entrada y enviarle
 * todas líneas al proceso hijo y almacenar en el fichero
 * todas las salidas (0.5p).
 * Recuerda que ProcessBuilder tiene métodos para realizar esta tarea:
 *
 * ej: java -jar Ejercicio4.jar <fichero_entrada> <fichero_salida>
 *
 * Contenido fichero entrada:
 * 1
 * 3
 * 6
 * stop
 *
 * Contenido fichero salida:
 * 1
 * 4
 * 10
 *
 */
public class Ejercicio4 {

    /**
     * Le pasa todos los números de <fichero_entrada>
     * a SumaMeHijo por la entrada estándar y el resultado
     * lo almacena en <fichero_salida>
     *
     * java -jar Ejercicio4.jar <fichero_entrada> <fichero_salida>
     *
     * @param args Dos strings con el nombre de los ficheros
     */
    public static void main(String[] args) {

        if (args.length != 2){
            System.err.println("Número de Parámetros Incorrecto");
            System.exit(1);
        }

        String entradatxt = args[0];
        String salidatxt = args[1];

        String commando = "java -jar SumaMeHijo.jar -i";

        ProcessBuilder pb = new ProcessBuilder(commando.split(" "));

        pb.redirectInput(new File(entradatxt));
        pb.redirectOutput(new File(salidatxt));

        try {
            Process process = pb.start();
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
